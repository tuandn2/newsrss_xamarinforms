﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Plugin.Connectivity;

namespace NewsRSS
{
    public class CloudDataStore : IDataStore<News>
    {
        HttpClient client;
        List<News> listNews;
        string dateFormat;
        public CloudDataStore()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"{App.BackendUrl}/");

            listNews = new List<News>();
        }

        public async Task<IEnumerable<News>> GetItemsAsync(bool forceRefresh = false)
        {
            if (forceRefresh && CrossConnectivity.Current.IsConnected)
            {
                var json = await client.GetStringAsync($"trangchu.rss");
            }

            return listNews;
        }

        public async Task<List<News>> GetItemAsync(string url)
        {
            if (!url.Equals("") && CrossConnectivity.Current.IsConnected)
            {
                switch(url){
                    case App.DANTRI:
                        dateFormat = "ddd, dd MMM yyyy HH:mm:ss 'GMT'z";
                        break;
                    case App.VNX:
                        dateFormat = "ddd, dd MMM yyyy HH:mm:ss zzz";
                        break;
                }
                try
                {
                    WebRequest webRequest = WebRequest.Create(url);
                    WebResponse webResponse = webRequest.GetResponse();
                    Stream stream = webResponse.GetResponseStream();
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(stream);
                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDocument.NameTable);
                    nsmgr.AddNamespace("dc", xmlDocument.DocumentElement.GetNamespaceOfPrefix("dc"));
                    nsmgr.AddNamespace("content", xmlDocument.DocumentElement.GetNamespaceOfPrefix("content"));
                    XmlNodeList itemNodes = xmlDocument.SelectNodes("rss/channel/item");

                    for (int i = 0; i < itemNodes.Count; i++)
                    {
                        News news = new News();

                        if (itemNodes[i].SelectSingleNode("title") != null)
                        {
                            news.Title = itemNodes[i].SelectSingleNode("title").InnerText;
                        }
                        if (itemNodes[i].SelectSingleNode("link") != null)
                        {
                            news.Link = itemNodes[i].SelectSingleNode("link").InnerText;
                        }
                        if (itemNodes[i].SelectSingleNode("pubDate") != null)
                        {
                            news.PubDate = DateTime.ParseExact(itemNodes[i].SelectSingleNode("pubDate").InnerText, dateFormat,System.Globalization.CultureInfo.InvariantCulture);
                        }
                        if (itemNodes[i].SelectSingleNode("description") != null)
                        {
                            String description = itemNodes[i].SelectSingleNode("description").InnerText;
                            news.Description = description;
                            var reg = new Regex("src=(?:\"|\')?(?<imgSrc>[^>]*[^/].(?:jpg|bmp|gif|png))(?:\"|\')?");
                            var match = reg.Match(description);
							if (match.Success)
							{
                                news.Img = match.Groups["imgSrc"].Value;
							}
                        }
                        listNews.Add(news);
                    }
                }
                catch (Exception)
                {
                    throw;
                }

            }

            return await Task.Run(() => listNews);
        }

        public async Task<bool> AddItemAsync(News news)
        {
            if (news == null || !CrossConnectivity.Current.IsConnected)
                return false;

            var serializedItem = JsonConvert.SerializeObject(news);

            var response = await client.PostAsync($"api/item", new StringContent(serializedItem, Encoding.UTF8, "application/json"));

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> UpdateItemAsync(News news)
        {
            if (news == null || news.Guid == null || !CrossConnectivity.Current.IsConnected)
                return false;

            var serializedItem = JsonConvert.SerializeObject(news);
            var buffer = Encoding.UTF8.GetBytes(serializedItem);
            var byteContent = new ByteArrayContent(buffer);

            var response = await client.PutAsync(new Uri($"api/item/{news.Guid}"), byteContent);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            if (string.IsNullOrEmpty(id) && !CrossConnectivity.Current.IsConnected)
                return false;

            var response = await client.DeleteAsync($"api/item/{id}");

            return response.IsSuccessStatusCode;
        }
    }
}
