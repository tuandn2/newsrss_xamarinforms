﻿using System;
namespace NewsRSS
{
    public class News
    {
        public string Guid { get; set; }
		public string Title { get; set; }
        public string Description { get; set; }
        public string Img { get; set; }
        public string Link { get; set; }
        public DateTime PubDate { get; set; }
    }
}
