﻿using System;

using Xamarin.Forms;

namespace NewsRSS
{
    public partial class ItemDetailPage : ContentPage
    {
        NewsDetailViewModel viewModel;

        // Note - The Xamarin.Forms Previewer requires a default, parameterless constructor to render a page.
        public ItemDetailPage()
        {
            InitializeComponent();

            var news = new News
            {
                Link = "http://google.com",
            };

            viewModel = new NewsDetailViewModel(news);
            BindingContext = viewModel;
        }

        public ItemDetailPage(NewsDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        void Handle_Navigating(object sender, Xamarin.Forms.WebNavigatingEventArgs e)
        {
            progress.IsVisible = true;
        }

        void Handle_Navigated(object sender, Xamarin.Forms.WebNavigatedEventArgs e)
        {
            progress.IsVisible = false;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await progress.ProgressTo(0.9, 900, Easing.SpringIn);
        }
    }
}
