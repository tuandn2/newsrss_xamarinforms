﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace NewsRSS
{
    public partial class ItemsPage : ContentPage
    {
        NewsViewModel viewModel;

        public ItemsPage(string newsUrl)
        {
            InitializeComponent();
            BindingContext = viewModel = new NewsViewModel(newsUrl);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var news = args.SelectedItem as News;
            if (news == null)
                return;

            await Navigation.PushAsync(new ItemDetailPage(new NewsDetailViewModel(news)));

            // Manually deselect item
            NewsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewItemPage());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.listNews.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
    }
}
