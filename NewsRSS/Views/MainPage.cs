﻿using System;

using Xamarin.Forms;

namespace NewsRSS
{
    public class MainPage : TabbedPage
    {
        
        public MainPage()
        {
            Page dantriPage, aboutPage, vnxPage = null;


            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    dantriPage = new NavigationPage(new ItemsPage(App.DANTRI))
                    {
                        Title = "Dân Trí"
                    };
                    vnxPage = new NavigationPage(new ItemsPage(App.VNX))
					{
						Title = "VnExpress"
					};
                    aboutPage = new NavigationPage(new AboutPage())
                    {
                        Title = "About"
                    };
                    dantriPage.Icon = "tab_feed.png";
                    vnxPage.Icon    = "tab_feed.png";
                    aboutPage.Icon  = "tab_about.png";
                    break;
                default:
                    dantriPage = new ItemsPage(App.DANTRI)
                    {
                        Title = "Dân Trí"
                    };
                    vnxPage = new ItemsPage(App.VNX)
					{
						Title = "VnExpress"
					};
                    aboutPage = new AboutPage()
                    {
                        Title = "About"
                    };
                    break;
            }

            Children.Add(dantriPage);
            Children.Add(vnxPage);
            Children.Add(aboutPage);

            Title = Children[0].Title;
        }

        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            Title = CurrentPage?.Title ?? string.Empty;
        }
    }
}
