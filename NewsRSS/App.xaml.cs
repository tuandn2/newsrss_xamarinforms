﻿using System;

using Xamarin.Forms;

namespace NewsRSS
{
    public partial class App : Application
    {
        public static bool UseMockDataStore = false;
        public static string BackendUrl = "http://dantri.com.vn";
        public const string DANTRI     = "http://dantri.com.vn/trangchu.rss";
		public const string VNX        = "https://vnexpress.net/rss/tin-moi-nhat.rss";


        public App()
        {
            InitializeComponent();

            if (UseMockDataStore)
                DependencyService.Register<MockDataStore>();
            else
                DependencyService.Register<CloudDataStore>();

            if (Device.RuntimePlatform == Device.iOS)
                MainPage = new MainPage();
            else
                MainPage = new NavigationPage(new MainPage());
        }
    }
}
