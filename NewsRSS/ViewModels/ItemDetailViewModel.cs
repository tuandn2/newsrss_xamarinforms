﻿using System;

namespace NewsRSS
{
    public class NewsDetailViewModel : BaseViewModel
    {
        public News newsData { get; set; }
        public NewsDetailViewModel(News news = null)
        {
            newsData = news;
        }
    }
}
