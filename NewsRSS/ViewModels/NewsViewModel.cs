﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using System.Collections.Generic;


namespace NewsRSS
{
    public class NewsViewModel : BaseViewModel
    {
        public ObservableCollection<News> listNews { get; set; }
		public Command LoadItemsCommand { get; set; }
        string url;

		public NewsViewModel(string url)
		{
            this.url = url;
            switch (url)
            {
                case App.DANTRI:
                    Title = "Báo Dân trí";
                    break;
                case App.VNX:
                    Title = "Báo VnExpess";
                    break;
            }
			
            listNews = new ObservableCollection<News>();
			LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
		}

		async Task ExecuteLoadItemsCommand()
		{
			if (IsBusy)
				return;

			IsBusy = true;

			try
			{
				listNews.Clear();
                List<News> list = await DataStore.GetItemAsync(url);
                foreach(var news in list){
                    listNews.Add(news);
                }
                IsBusy = false;
			
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
			}
			finally
			{
				IsBusy = false;
			}
		}
    }
}
